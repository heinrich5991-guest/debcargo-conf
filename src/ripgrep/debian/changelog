rust-ripgrep (13.0.0-2) unstable; urgency=medium

  * Package ripgrep 13.0.0 from crates.io using debcargo 2.4.4-alpha.0
  * Install zsh (Closes: #993354)

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 31 Aug 2021 22:06:04 +0200

rust-ripgrep (13.0.0-1) unstable; urgency=medium

  * Package ripgrep 13.0.0 from crates.io using debcargo 2.4.4-alpha.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 21 Aug 2021 20:36:44 +0200

rust-ripgrep (12.1.1-1) unstable; urgency=medium

  * Package ripgrep 12.1.1 from crates.io using debcargo 2.4.3

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 20 Jun 2020 10:59:18 +0200

rust-ripgrep (12.1.0-1) unstable; urgency=medium

  * Package ripgrep 12.1.0 from crates.io using debcargo 2.4.2

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 10 May 2020 16:48:40 +0200

rust-ripgrep (12.0.1-1) unstable; urgency=medium

  * Package ripgrep 12.0.1 from crates.io using debcargo 2.4.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Thu, 02 Apr 2020 11:14:36 +0200

rust-ripgrep (12.0.0-1) unstable; urgency=medium

  * Package ripgrep 12.0.0 from crates.io using debcargo 2.4.0

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 28 Mar 2020 12:18:49 +0100

rust-ripgrep (11.0.2-1) unstable; urgency=medium

  * Package ripgrep 11.0.2 from crates.io using debcargo 2.4.0
    (Closes: #945021, #925544)
  * Remove the jemallocator dependencies (still in NEW)
    disable-jemallocator.diff
  * Improve the description (Closes: #925603)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sun, 01 Dec 2019 12:16:32 +0100

rust-ripgrep (0.10.0-2) unstable; urgency=medium

  * Package ripgrep 0.10.0 from crates.io using debcargo 2.2.10
  * Force grep-searcher to be at least >= 0.1.3 to avoid bug #916615
  * Disable Windows specific test (closes: #922099)
    https://github.com/BurntSushi/ripgrep/commit/a4868b88351318182eed3b801d0c97a106a7d38f

  [ Peter Michael Green ]
  * Use a temporary directory under the package build directory
    (avoids issues when multiple people build the package on the same machine)
    (Closes: #921693)

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 12 Feb 2019 09:25:39 +0100

rust-ripgrep (0.10.0-1) unstable; urgency=medium

  * Package ripgrep 0.10.0 from crates.io using debcargo 2.2.7
    (Closes: #913893)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 15 Dec 2018 11:20:34 +0100

rust-ripgrep (0.9.0-6) unstable; urgency=medium

  * Also ship zsh & fish completions (Closes: #908807)

 -- Sylvestre Ledru <sylvestre@debian.org>  Fri, 14 Sep 2018 10:25:24 +0200

rust-ripgrep (0.9.0-5) unstable; urgency=medium

  * Team upload.
  * Package ripgrep 0.9.0 from crates.io using debcargo 2.2.7

 -- Ximin Luo <infinity0@debian.org>  Thu, 13 Sep 2018 00:11:50 -0700

rust-ripgrep (0.9.0-4) unstable; urgency=medium

  [ Helmut Grohne ]
  * Support DEB_BUILD_OPTIONS=nocheck. (Closes: #907739)

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 03 Sep 2018 11:10:29 +0200

rust-ripgrep (0.9.0-3) unstable; urgency=medium

  * Run tests at build phase
  * Add docbook-xsl to explicit dep to fix a manpage generation issue

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 14 Aug 2018 23:19:13 +0200

rust-ripgrep (0.9.0-2) unstable; urgency=medium

  * Package ripgrep 0.9.0 from crates.io using debcargo 2.2.5
  * Add man pages and bash completion scripts, and improve package description.

 -- Ximin Luo <infinity0@debian.org>  Sat, 04 Aug 2018 13:06:34 -0700

rust-ripgrep (0.9.0-1) unstable; urgency=medium

  * Package ripgrep 0.9.0 from crates.io using debcargo 2.2.5

  [ Sylvestre Ledru ]
  * Package ripgrep 0.8.1 from crates.io using debcargo 2.2.3

 -- Ximin Luo <infinity0@debian.org>  Sat, 04 Aug 2018 10:22:15 -0700
