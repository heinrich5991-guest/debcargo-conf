rust-sequoia-openpgp (1.8.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 1.8.0 from crates.io using debcargo 2.5.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 11 Apr 2022 20:53:35 -0700

rust-sequoia-openpgp (1.7.0-6) unstable; urgency=medium

  * Package sequoia-openpgp 1.7.0 from crates.io using debcargo 2.5.0
  * fix time_t handling on 32-bit platforms
  * mark compression serialization test as requiring bzip2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 10 Feb 2022 10:30:08 -0500

rust-sequoia-openpgp (1.7.0-5) unstable; urgency=medium

  * Package sequoia-openpgp 1.7.0 from crates.io using debcargo 2.5.0
  * Consolidate workarounds for #985741 and #985762

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 10 Feb 2022 00:44:24 -0500

rust-sequoia-openpgp (1.7.0-4) unstable; urgency=medium

  * Package sequoia-openpgp 1.7.0 from crates.io using debcargo 2.5.0
  * Adjust autopkgtests to align with upstream feature requirements

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 09 Feb 2022 18:02:15 -0500

rust-sequoia-openpgp (1.7.0-3) unstable; urgency=medium

  * Package sequoia-openpgp 1.7.0 from crates.io using debcargo 2.5.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 09 Feb 2022 01:56:40 -0500

rust-sequoia-openpgp (1.7.0-2) unstable; urgency=medium

  * Package sequoia-openpgp 1.7.0 from crates.io using debcargo 2.5.0
  * Move to unstable from experimental

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 05 Feb 2022 20:57:31 -0500

rust-sequoia-openpgp (1.7.0-1) experimental; urgency=medium

  * Package sequoia-openpgp 1.7.0 from crates.io using debcargo 2.5.0
  * refresh patches dropping windows build and criterion benchmarks
  * also drop wasm build, associated rust crypto, and experimental or
    variable-time crypto features (debian build is still focusd on nettle)
  * Upstream license changed from GPL-2.0+ to LGPL-2.0+, debian packaging
    followed suit.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 02 Feb 2022 03:26:36 -0500

rust-sequoia-openpgp (1.3.0-4) unstable; urgency=medium

  * Package sequoia-openpgp 1.3.0 from crates.io using debcargo 2.4.4
  * Remove compression layer from some tests so that compression-free
    feature set can test successfully.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 31 Aug 2021 10:12:27 -0400

rust-sequoia-openpgp (1.3.0-3) unstable; urgency=medium

  * Package sequoia-openpgp 1.3.0 from crates.io using debcargo 2.4.4
  * Update debugging/diagnosis patch supplied by upstream

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 30 Aug 2021 09:12:14 -0400

rust-sequoia-openpgp (1.3.0-2) unstable; urgency=medium

  * Package sequoia-openpgp 1.3.0 from crates.io using debcargo 2.4.4
  * Apply debugging/diagnosis patch supplied by upstream

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sun, 29 Aug 2021 13:17:56 -0400

rust-sequoia-openpgp (1.3.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 1.3.0 from crates.io using debcargo 2.4.4

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Thu, 19 Aug 2021 00:43:40 -0400

rust-sequoia-openpgp (1.1.0-3) unstable; urgency=medium

  * Package sequoia-openpgp 1.1.0 from crates.io using debcargo 2.4.4
  * Drop test-skipping workaround for #985729 and #985730, adopt
    upstream's workaround instead
  * fix test suite on platforms with 32-bit time_t

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 23 Mar 2021 12:28:45 -0400

rust-sequoia-openpgp (1.1.0-2) unstable; urgency=medium

  * Package sequoia-openpgp 1.1.0 from crates.io using debcargo 2.4.4
  * Drop unncessary config about obsolete "quickcheck" feature
  * Clarify which autopkgtests should succeed (including workaround
    for debcargo's #985762)
  * Work around #985729 and #985730 by dropping some regex tests

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 22 Mar 2021 19:42:47 -0400

rust-sequoia-openpgp (1.1.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 1.1.0 from crates.io using debcargo 2.4.4
  * ship example rust source files

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 05 Mar 2021 18:02:55 -0500

rust-sequoia-openpgp (1.0.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 1.0.0 from crates.io using debcargo 2.4.3

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 16 Dec 2020 11:24:54 -0500

rust-sequoia-openpgp (0.21.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.21.0 from crates.io using debcargo 2.4.3
  * drop most dependency cleanup, already upstream

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 14 Dec 2020 22:27:10 -0500

rust-sequoia-openpgp (0.20.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.20.0 from crates.io using debcargo 2.4.3

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 14 Oct 2020 23:59:39 -0400

rust-sequoia-openpgp (0.18.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.18.0 from crates.io using debcargo 2.4.2
  * relax MSRV-related dependency bounds on dyn-clone

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 05 Aug 2020 19:29:31 -0400

rust-sequoia-openpgp (0.17.0-5) unstable; urgency=medium

  * Package sequoia-openpgp 0.17.0 from crates.io using debcargo 2.4.2
  * actually mark the correct autopkgtests as broken

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 23 Jun 2020 13:04:52 -0400

rust-sequoia-openpgp (0.17.0-4) unstable; urgency=medium

  * Package sequoia-openpgp 0.17.0 from crates.io using debcargo 2.4.2
  * Mark autopkgtests as broken for features with dependencies that cargo
    cannot currently represent

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 22 Jun 2020 12:48:37 -0400

rust-sequoia-openpgp (0.17.0-3) unstable; urgency=medium

  * Package sequoia-openpgp 0.17.0 from crates.io using debcargo 2.4.2
  * Adjust dependencies: crypto-nettle is a required backend, and the
    quickcheck flavor also depends on rand.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 20 Jun 2020 13:13:14 -0400

rust-sequoia-openpgp (0.17.0-2) unstable; urgency=medium

  * Package sequoia-openpgp 0.17.0 from crates.io using debcargo 2.4.2
  * No-op source-only re-upload for Debian Testing Migration.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 19 Jun 2020 16:00:54 -0400

rust-sequoia-openpgp (0.17.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.17.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 17 Jun 2020 17:15:32 -0400

rust-sequoia-openpgp (0.16.0-4) unstable; urgency=medium

  * Package sequoia-openpgp 0.16.0 from crates.io using debcargo 2.4.2
  * accept building against base64 0.12.*

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Wed, 10 Jun 2020 13:09:57 -0400

rust-sequoia-openpgp (0.16.0-3) unstable; urgency=medium

  * Unbreak autopkg test with additional upstream fix
  * Package sequoia-openpgp 0.16.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 28 Apr 2020 11:47:56 -0400

rust-sequoia-openpgp (0.16.0-2) unstable; urgency=medium

  * Avoid autopkg tests that require compression when using
    --no-default-features (backporting upstream fix)
  * Package sequoia-openpgp 0.16.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 27 Apr 2020 10:28:49 -0400

rust-sequoia-openpgp (0.16.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.16.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 24 Apr 2020 15:27:44 -0400

rust-sequoia-openpgp (0.15.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.15.0 from crates.io using debcargo 2.4.2
  * Cherry-pick an upstream patch to avoid running padding example without
    compression-deflate feature.

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Fri, 06 Mar 2020 14:06:56 -0500

rust-sequoia-openpgp (0.14.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.14.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 18 Feb 2020 18:55:02 -0500

rust-sequoia-openpgp (0.13.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.13.0 from crates.io using debcargo 2.4.2

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Tue, 14 Jan 2020 17:32:40 -0500

rust-sequoia-openpgp (0.12.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.12.0 from crates.io using debcargo 2.4.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Mon, 25 Nov 2019 23:41:12 -0500

rust-sequoia-openpgp (0.11.0-1) unstable; urgency=medium

  * Package sequoia-openpgp 0.11.0 from crates.io using debcargo 2.4.0

 -- Daniel Kahn Gillmor <dkg@fifthhorseman.net>  Sat, 09 Nov 2019 23:41:01 -0500
